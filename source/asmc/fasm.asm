  ;; flat assembler interface for asmc
  ;; Copyright (C) Giovanni Mascellani <gio@debian.org>
  ;; Distributed under the same terms of fasm core

  memory_size dd 0x100000
  additional_memory_size dd 0x100000

  align 4

  return_code dd ?

  jump_buffer rb 36

  malloc_addr dd ?
  free_addr dd ?
  setjmp_addr dd ?
  longjmp_addr dd ?
  log_addr dd ?
  open_addr dd ?
  create_addr dd ?
  read_addr dd ?
  write_addr dd ?
  close_addr dd ?
  lseek_addr dd ?
  fatal_error_addr dd ?
  assembler_error_addr dd ?
  display_block_addr dd ?
  get_environment_variable_addr dd ?
  make_timestamp_addr dd ?


main:
  push ebp
  mov ebp, esp

  ;; Save parameters
  mov ebx, [ebp+8]
  mov eax, [ebx]
  mov [input_file], eax
  add ebx, 4
  mov eax, [ebx]
  mov [output_file], eax
  add ebx, 4
  mov eax, [ebx]
  mov [malloc_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [free_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [setjmp_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [longjmp_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [log_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [open_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [create_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [read_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [write_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [close_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [lseek_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [fatal_error_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [assembler_error_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [display_block_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [get_environment_variable_addr], eax
  add ebx, 4
  mov eax, [ebx]
  mov [make_timestamp_addr], eax
  add ebx, 4

  call fix_tables

  call init_memory

  mov word [passes_limit], 10
  mov word [current_pass], 0

  ;; Save the current stack location
  mov eax, [setjmp_addr]
  push jump_buffer
  call eax
  add esp, 4

  test eax, eax
  jz run_fasm

  mov eax, [return_code]
  pop ebp
  ret


run_fasm:
  ;; Here we call the routines that do the heavy lifting
  call preprocessor
  call parser
  call assembler
  call formatter

  mov eax, 0
  call exit_program

init_memory:
  ;; Allocate memory
  mov eax, [malloc_addr]
  push dword [memory_size]
  call eax
  add esp, 4
  mov [memory_start], eax
  add eax, [memory_size]
  mov [memory_end], eax

  mov eax, [malloc_addr]
  push DWORD [additional_memory_size]
  call eax
  add esp, 4
  mov [additional_memory], eax
  add eax, [additional_memory_size]
  mov [additional_memory_end], eax

  ret


exit_program:
  ;; Return code in AL
  movzx eax, al
  mov [return_code], eax

  ;; Deallocate memory
  mov eax, [free_addr]
  push dword [memory_start]
  call eax
  add esp, 4

  mov eax, [free_addr]
  push dword [additional_memory]
  call eax
  add esp, 4

  ;; Long jump to saved location
  mov eax, [longjmp_addr]
  push 1
  push jump_buffer
  call eax


fix_tables:
  mov esi, symbols
  mov edi, 3
  mov ebx, 14
  call fix_table

  mov esi, instructions
  mov edi, 5
  mov ebx, 20
  call fix_table

  mov esi, data_directives
  mov edi, 5
  mov ebx, 8
  call fix_table

  ret


fix_table:
fix_table_loop:
  cmp edi, ebx
  je fix_table_ret
  movzx eax, word [esi+2]
  xor edx, edx
  div edi
  mov word [esi+2], ax
  add esi, 4
  inc edi
  jmp fix_table_loop
fix_table_ret:
  ret
