  ;; flat assembler interface for asmc
  ;; Copyright (C) Giovanni Mascellani <gio@debian.org>
  ;; Distributed under the same terms of fasm core

zero_to_carry:
  test eax, eax
  jz zero_to_carry_carry
  clc
  jmp zero_to_carry_ret
zero_to_carry_carry:
  stc
zero_to_carry_ret:
  ret

open:
  ;; Filename in EDX
  ;; Return handle in EBX and clear CF to indicate success
  push eax
  push ecx
  push edx
  push esi
  push edi
  push ebp

  push edx
  mov eax, [open_addr]
  call eax
  add esp, 4
  mov ebx, eax

  call zero_to_carry

  pop ebp
  pop edi
  pop esi
  pop edx
  pop ecx
  pop eax

  ret


create:
  ;; Filename in EDX
  ;; Return handle in EBX and clear CF to indicate success
  push eax
  push ecx
  push edx
  push esi
  push edi
  push ebp

  push edx
  mov eax, [create_addr]
  call eax
  add esp, 4
  mov ebx, eax

  call zero_to_carry

  pop ebp
  pop edi
  pop esi
  pop edx
  pop ecx
  pop eax

  ret


read:
  ;; Read ECX bytes from handle EBX into the buffer pointed by EDX
  ;; Clear CF to indicate success
  push eax
  push ebx
  push ecx
  push edx
  push esi
  push edi
  push ebp

  push ebx
  push edx
  push ecx
  mov eax, [read_addr]
  call eax
  add esp, 12

  call zero_to_carry

  pop ebp
  pop edi
  pop esi
  pop edx
  pop ecx
  pop ebx
  pop eax

  ret


write:
  ;; Write ECX bytes to handle EBX from the buffer pointed by EDX
  ;; Clear CF to indicate success
  push eax
  push ebx
  push ecx
  push edx
  push esi
  push edi
  push ebp

  push ebx
  push edx
  push ecx
  mov eax, [write_addr]
  call eax
  add esp, 12

  call zero_to_carry

  pop ebp
  pop edi
  pop esi
  pop edx
  pop ecx
  pop ebx
  pop eax

  ret


close:
  ;; Close handle in EBX
  push eax
  push ebx
  push ecx
  push edx
  push esi
  push edi
  push ebp

  push ebx
  mov eax, [close_addr]
  call eax
  add esp, 4

  pop ebp
  pop edi
  pop esi
  pop edx
  pop ecx
  pop ebx
  pop eax

  ret


lseek:
  ;; Seek handle in EBX to position EDX with respect to flag in AL (0 -> beginning, 1 -> current, 2 -> end)
  ;; Return position from beginning in EAX
  push ebx
  push ecx
  push edx
  push esi
  push edi
  push ebp

  movzx eax, al
  push ebx
  push edx
  push eax
  mov eax, [lseek_addr]
  call eax
  add esp, 12

  pop ebp
  pop edi
  pop esi
  pop edx
  pop ecx
  pop ebx

  ret


get_environment_variable:
  ;; Search the variable named after the buffer pointed by ESI and store its value in the buffer pointed by EDI
  ;; Adjust EDI so that it points to the byte just after the returned value
  ;; Do not touch EDI if the variable does not exist
  push eax
  push ebx
  push ecx
  push edx
  push esi
  push edi
  push ebp

  push esi
  mov eax, [get_environment_variable_addr]
  call eax
  add esp, 4

  pop ebp
  pop edi
  pop esi
  pop edx
  pop ecx
  pop ebx
  pop eax

  ret


make_timestamp:
  ;; Return the UNIX timestamp in EAX
  push ebx
  push ecx
  push edx
  push esi
  push edi
  push ebp

  mov eax, [make_timestamp_addr]
  call eax

  pop ebp
  pop edi
  pop esi
  pop edx
  pop ecx
  pop ebx

  ret


fatal_error:
  ;; Message pointed by the pointer pointed by ESP
  push dword [esp]
  mov eax, [fatal_error_addr]
  call eax
  add esp, 4

  mov al, 255
  call exit_program


assembler_error:
  ;; Message pointed by the pointer pointed by ESP, plus some other protocol
  push dword [esp]
  mov eax, [assembler_error_addr]
  call eax
  add esp, 4

  mov al, 2
  call exit_program


display_block:
  ;; Print the data in the buffer pointed by ESI of length ECX
  push ebx

  push esi
  push ecx
  mov eax, [display_block_addr]
  call eax
  add esp, 8

  pop ebx

  ret
